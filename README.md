# Luftgut Webseite

<a href="https://luft-gut.de" target="_blank">
  <img src="https://screenshotapi-dot-net.storage.googleapis.com/luft_gut_de_fqy8g3xcgw1b.png" alt="Website Screenshot" width="60%">
</a>
<br>
<br>
<a href="https://luft-gut.de" target="_blank">
  <img src="https://img.shields.io/badge/Webseite-luft--gut.de-success" alt="Website Shield">
</a>

Die `master` branch ist die Produktionsbranch und wird automatisch auf die eigentliche Webseite übertragen. Bevor commits zu der `master` branch hinzugefügt werden, sollten diese zuvor in der `development` branch getestet werden. Dafür erstellt Vercel (Webhostingprovider) eine eigene URL bereit, welche unter dem Menüpunkt `CI / CD` > `Jobs` aus dem Log ausgelesen werden kann.

## Development Setup

<a href="https://v2.parceljs.org" target="_blank">
  <img src="https://v2.parceljs.org/assets/parcel@2x.png" alt="Parcel.js Logo" width="34px">
  <img src="https://v2.parceljs.org/assets/logo.svg" alt="Parcel.js Logo" width="100px">
</a>

Um schneller und flexibler programmieren zu können verfügt die Website über einen Module Bundler - [Parcle.js](https://v2.parceljs.org). Damit dieser jedoch richtig funktioniert muss zunächst NodeJS auf dem Rechner installiert werden.

😅 **Was ist ein Module Bundler?**: Ein Module Bundler ist ein Tool welches bei der Frontendentwicklung von Webseiten zum Einsatz kommt. Da der Browser keine Javascript Module zulässt wird ein Module Bundler verwendet um die für Programmierer gut zugänglichen Javascript Module in eine für den Browser verständliche Javascript Datei zu verpacken. Häufig bieten Module Bundler zudem die Möglichkeit unterschiedlich Funktionen auf den Code anzuwenden. Beispielsweise gibt es eine Funktion bei der, der Code von sämtlichen Whitespaces befreit wird, um so schneller zu laden und weniger Datenvolumen zu beanspruchen -> Diese Funktion is als Minifying bekannt.

Mehr Infos zu Module Bundlern [Module Bundlers Explained - YouTube](https://www.youtube.com/watch?v=5IG4UmULyoA)

### NodeJS

Für jedes Betriebssystem gibt es mehrere, verschiedene Wege NodeJS zu installieren.

**1. Möglichkeit: Direkter Download**

- **Windows und macOS**:<br>
  Unter Windows und macOS können Installer direkt von der NodeJS Webseite installiert werden.<br>
  [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

**2. Möglichkeit: Paketverwaltungen**

- **Windows - 🍫 Chocolatey**:

  ```bash
  choco install nodejs
  ```

- **macOS - 🍺 Homebrew**:

  ```bash
  brew install nodejs
  ```

- **Linux 🐧**:
  ```bash
  sudo apt install nodejs
  ```

Ist NodeJS erfolgreich installiert sollte der Befehl die aktuell installierte NodeJS Version ausgeben.

```bash
node --version
```

### NPM

Mit NodeJS wurde auch gleich der Node Package Manager (NPM) installiert, welcher verwendet wird um die für die Website nötigen Packete (bspw. Firebase) herunterzuladen.

😮 **Was ist NPM?**: NPM ist eine Packetverwaltung. Eine Paketverwaltung hostet von Nutzern erstelle Bibliotheken welche bei NodeJS einfach über den `npm` ([Node Package Manager](https://www.npmjs.com)) Befehl installiert und verwendet werden können.

1. **Navigiere zum Projekt**<br>

   ```bash
   cd <path-to-project>
   ```

2. **Packete installieren**<br>

   ```bash
   npm install
   ```

### VSCode

Das Projekt kommt mit VSCode Voreinstellungen und Pluginempfehlung.

- **Format on Safe** sorgt dafür, dass automatisch der passende Codestyle eingehalten wird. Dafür ist lediglich das [Prettier Plugin](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) für VSCode aus dem Marktplatz herunterzuladen.

🤨 **Was macht Prettier?**: Prettier ist _Code Formatter_. Ein Code Formatter sorgt dafür das der geschriebene Code einem zuvor festgeleten Style folgt. VSCode ist so eingestellt, dass bei jedem Speicheraufruf das Dokument automatisch an den festgelegten Codestyle angepasst wird. Das Ergebniss ist konsistent gleich aussehender Code, was bei großen Projekten, mit vielen Entwickleren, sehr nützlich ist.

## Ausführen

### Development

Dies startet einen Entwicklungsserver welcher über die angeziegt URL aufgerufen werden kann. Nimmt man Änderungen im Code vor werden diese Automatisch auf der lokalen Webseite wiedergespiegelt.

```bash
npm start
```

### Production

**Allgemein**

Möchte man die komplierten Dateien erhalten ist lediglich der npm build Prozess auszuführen.

```shell
npm run build
```

Damit generiert Parcel einen neune Ordner `./dist` (Distribution), in welchen sämtliche für die Webseite relevanten Dateien erzeugt werden.

**Docker**

Möchte man die Webseite in einem Docker Container ausführen steht ein Dockerfile zur Verfügung.

Dieses kann mit folgendem Befehl in ein Docker Image kompiliert werden.

```shell
docker build --tag luftgut/website .
```

Ist das Image erstellt kann ein neuer Container mit folgendem Befehl erstellt werden.

```shell
docker run -d -p 8080:80 --name my-luftgut-website luftgut/website
```

Dies erstellt einen [NGINX](https://www.nginx.com) Server mit dem Label `my-luftgut-server` und routet den Traffix von Port `8080`, auf dem Host, zu Port `80` in dem Container.

Die `-d` Flagge sorgt dafür, dass der Container im Hintergrund (_detached_) ausgeführt wird.

## CI / CD

Im Hintergrund dieses Repository läuft ein Azure Server welcher bei jedem neuen Push auf die `master` oder die `development` branch die Webseite aktulisiert. Den genauen Ablauf für das bereitstellen vía Vercel regelet die `.gitlab-ci.yml` Datei.

Eventuell kann es zu Verzögerungen bei der Ausführung der Pipelines kommen, da dieser Aufgabe nur ein sehr kleiner (schwacher) Server zur Verfügung steht.

Fix für [#5 - lookup docker on 168.63.129.16:53: no such host](https://gitlab.ei.hs-duesseldorf.de/wetter-station-gruppe-e/luftgut-website/-/issues/5):
[GitLab Runner Einrichten](https://gitlab.ei.hs-duesseldorf.de/wetter-station-gruppe-e/plan/-/blob/master/GITLAB-RUNNER.md)
