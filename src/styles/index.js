/**
 * normalize.css is a css preset that ensures a consistant style
 * standard across all browsers.
 */
import 'normalize.css'

/**
 * Import own css, scss or sass files
 */
import './page.css'
import './tag.sass'
