// === IMPORTS ===
import $ from 'jquery'
import { firebase, db, stations, datapoints } from './firebase'

// === YOUR FIREBASE CODE ===

// Get jquery element reference for firebaseSampleField
const $tag = $('#firebaseSampleField')

// Download a snapshot of the stations collection
stations.get().then(docs => {
  // For each station inside the collection print its id and its location
  docs.forEach(doc => {
    console.log(`Station [${doc.id}]: ${doc.data().location}`)
  })

  // Update the firebaseSampleField label
  if (docs.size !== 0) {
    // Found one or more stations
    $tag.html(`${docs.size} Stationen gefunden`)
    $tag.addClass('success')
  } else {
    // No stations found
    $tag.html(`Keine Stationen gefunden`)
    $tag.addClass('failure')
  }
})
