# Based on:
# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
# https://blog.maximerouiller.com/post/how-to-build-a-multistage-dockerfile-for-spa-and-static-sites/

# Build the Dockerfile with: `docker build --tag luftgut-website .`

# === BUILD STAGE ===

# Run Base Image
FROM node:lts-alpine as build-stage

# Copy NodeJS Files
# See http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/ to why this gets done separatly.
WORKDIR /app
COPY package*.json ./

# Install Node Dependencies
# `ci` is more reliable and faster than `install`
RUN npm ci

# Copy remaining files
COPY . .

# Run Build
RUN npm run build


# === PRODUCTION STAGE ===

# Run Base Image
FROM nginx:stable-alpine as production-stage

# Copy Distribution Files
COPY --from=build-stage /app/dist /usr/share/nginx/html

# Expose Port 80
EXPOSE 80

# Start NGINX server
CMD ["nginx", "-g", "daemon off;"]
